
### IREuse 命令的编写思路

1. 数据文件存放于 Gitee
2. **ireuse.ado** 程序的编写参见 **bcuse.ado**, **cmaxuse.ado** 等外部命令的程序
3. 回头可以把这个程序作为 `cnuse` 命令的一个子程序。

### IREuse 命令的语法架构
- `ireuse`: 不附加任何选项，则在结果窗口中列出所有数据的名称和说明，一个包含超链接的列表。形式上类似于 `help bcuse` 帮助文件中提供的 [[Wooldridge data sets]](http://fmwww.bc.edu/ec-p/data/wooldridge/datasets.list.html)
- `ireuse dataname`: 从 Gitee 上自动下载 **dataname.dta** 文件到当前工作路径下