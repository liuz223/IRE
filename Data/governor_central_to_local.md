中央到地方交流任职的省长省委书记
================================

数据说明
--------

-   京官列表提供1978-2005年全国31个省（自治区、直辖市），从中央部委到地方任职的省长省委书记交流信息。期间共发生了49次京官交流，具体数据描述详见《中国地方官员治理的增长绩效》一书。

-   发布时间：2010-1-5

-   引用规则：徐现祥、王贤彬，2001：《地方官员培养与经济增长》收录到《中国地方官员治理的增长绩效》，科学出版社。
